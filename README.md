# Bulls Eye
**Bulls Eye**, the on target PHP Mail Server. The **Bulls Eye** mail server is based on the [Modoboa email server](https://modoboa.org/en/), which is a free and open-source mail hosting and management platform designed to work with Postfix SMTP server and Dovecot IMAP/POP3 server.

The original tool is great and I use it often, but I'm not friends with python, I like PHP - point. So I decided I wanted the tool, but I wanted it in PHP. To do this (for me) difficult task somewhat lighter as far as coding goes, I decided to use the latest Symfony Framework, with the *--full* option being used as the launchpad. 

## Features (being taken over)
  *  Modoboa by default uses Nginx web server to serve the webmail client and web-based admin panel.
  *  Compatible with Postfix and Dovecot.
  *  Support MySQL/MariaDB, or PostgreSQL database.
  *  Easily create unlimited mailboxes and unlimited mail domains in a web-based admin panel.
  *  Easily create email alias in the web-based admin panel.
  *  The webmail client provides an easy-to-use message filter to help you organize messages to different folders.
  *  It can help you protect your domain reputation by monitoring email blacklists and generating DMARC reports, so your emails have a better chance to land in the inbox instead of the spam folder.
  *  Includes amavis frontend to block spam and detect viruses in email.
  *  Calendar and address book.
  *  Integration with Let’s Encrypt.
  *  Includes AutoMX to allow end-users to easily configure mail account in a desktop or mobile mail client.


## License
[ISC License](LICENSE)

A permissive license lets people do anything with your code with proper attribution and without warranty. The ISC license is functionally equivalent to the BSD 2-Clause and MIT licenses, removing some language that is no longer necessary.

**Permissions**
- Commercial use
- Distribution
- Modification
- Private use

**Limitations**
- Liability
- Warranty

**Conditions**
- License and copyright notice
-   A copy of the license and copywrite notice must be included with the software.


## Docs
Check the [wiki](../../wikis/home/) or check every now and then for links added here.